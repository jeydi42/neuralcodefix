import random
import sys
from os import makedirs
from os.path import join, exists

import numpy as np
import torch
from torch import optim, nn
from torch.utils.tensorboard import SummaryWriter
from torchtext.data import Field, TabularDataset, BucketIterator
from torchtext.data.metrics import bleu_score

from inference import display_attention, create_attention_figure, compute_metrics_mtl, translate_sentence_mtl
from models.attention import Attention
from models.decoder import Decoder, SimpleDecoder
from models.encoder import Encoder
from models.seq2seq import Seq2MTLSeq
from training import evaluate_mtl, train_mtl
from utils.metrics import corrects

SEED = 1337

project = sys.argv[1]
size = sys.argv[2]
exp = sys.argv[3]
experiment_name = f"{project}-{size}-{exp}-MTL"
lr = 0.0002
N_EPOCHS = 200
BATCH_SIZE = 32

dataset_path = f"datasets/tufano/{project}/"

if not exists('best_models'):
    makedirs('best_models')

writer = SummaryWriter(f"runs/{experiment_name}")

random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

CODE = Field(init_token='<sos>',
             eos_token='<eos>',
             lower=True,
             include_lengths=True)

OP = Field(init_token='<sos>',
           eos_token='<eos>',
           lower=False,
           include_lengths=True)

train_data = TabularDataset(
    path=join(dataset_path, f'train_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE),
            'operations': ('op', OP)
            })

CODE.build_vocab(train_data, min_freq=1)
OP.build_vocab(train_data, min_freq=1)

valid_data = TabularDataset(
    path=join(dataset_path, f'eval_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE),
            'operations': ('op', OP)
            })

test_data = TabularDataset(
    path=join(dataset_path, f'test_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE),
            'operations': ('op', OP)
            })

train_iterator, valid_iterator, test_iterator = BucketIterator.splits(
    (train_data, valid_data, test_data),
    batch_size=BATCH_SIZE,
    sort_within_batch=True,
    sort_key=lambda x: len(x.src),
    device=device, shuffle=True)

INPUT_DIM = len(CODE.vocab)
OUTPUT_DIM = len(CODE.vocab)
OUTPUT_DIM_OP = len(OP.vocab)
ENC_EMB_DIM = 512
DEC_EMB_DIM = 512
ENC_HID_DIM = 256
DEC_HID_DIM = 256
ENC_DROPOUT = 0.2
DEC_DROPOUT = 0.2
SRC_PAD_IDX = CODE.vocab.stoi[CODE.pad_token]

attn_code = Attention(ENC_HID_DIM, DEC_HID_DIM)
attn_op = Attention(ENC_HID_DIM, DEC_HID_DIM)
enc = Encoder(INPUT_DIM, ENC_EMB_DIM, ENC_HID_DIM, DEC_HID_DIM, ENC_DROPOUT)
dec_code = Decoder(OUTPUT_DIM, DEC_EMB_DIM, ENC_HID_DIM, DEC_HID_DIM, DEC_DROPOUT, attn_code)
dec_op = SimpleDecoder(OUTPUT_DIM_OP, DEC_EMB_DIM, ENC_HID_DIM, DEC_HID_DIM, DEC_DROPOUT, attn_op)

model = Seq2MTLSeq(enc, dec_code, dec_op, SRC_PAD_IDX, device).to(device)


# writer.add_hparams({
#     "lr": lr, "batch": BATCH_SIZE, "project": project, "size": size, "ENC_EMB_DIM": ENC_EMB_DIM,
#     "DEC_EMB_DIM": DEC_EMB_DIM,
#     "ENC_HID_DIM": ENC_HID_DIM,
#     "DEC_HID_DIM": DEC_HID_DIM,
#     "ENC_DROPOUT": ENC_DROPOUT,
#     "DEC_DROPOUT": ENC_DROPOUT}, {})

def init_weights(m):
    for name, param in m.named_parameters():
        if 'weight' in name:
            nn.init.normal_(param.data, mean=0, std=0.01)
        else:
            nn.init.constant_(param.data, 0)


model.apply(init_weights)


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


print(f'The model has {count_parameters(model):,} trainable parameters')

optimizer = optim.Adam(model.parameters(), lr=lr, eps=0.0000008)
TRG_PAD_IDX = CODE.vocab.stoi[CODE.pad_token]
OP_PAD_IDX = CODE.vocab.stoi[OP.pad_token]

assert TRG_PAD_IDX == OP_PAD_IDX

criterion_code = nn.CrossEntropyLoss(ignore_index=TRG_PAD_IDX)
criterion_op = nn.CrossEntropyLoss(ignore_index=OP_PAD_IDX)

CLIP = 1

best_valid_loss = float('inf')
max_valid_corr = 0
best_valid_total_loss = float('inf')

example_idx = random.randint(1, len(train_data))

src = train_data.examples[example_idx].src
trg = train_data.examples[example_idx].trg
op = train_data.examples[example_idx].op
writer.add_text("Source", " ".join(src), 0)
writer.add_text("Target", " ".join(trg), 0)
writer.add_text("Operations", " ".join(op), 0)

max_lens = {"medium": 100, "small": 50}

train_metrics = {"BLUE": bleu_score, "Corrects": corrects}
for epoch in range(N_EPOCHS):

    train_loss_total, train_loss_1, train_loss_2 = train_mtl(model, train_iterator, optimizer, criterion_code,
                                                             criterion_op, CLIP)
    valid_loss_total, valid_loss_1, valid_loss_2 = evaluate_mtl(model, valid_iterator, criterion_code, criterion_op)
    test_loss_total, test_loss_1, test_loss_2 = evaluate_mtl(model, test_iterator, criterion_code, criterion_op)

    writer.add_scalar("Loss_Total/Train", train_loss_total, epoch)
    writer.add_scalar("Loss_Total/Valid", valid_loss_total, epoch)
    writer.add_scalar("Loss_Total/Test", test_loss_total, epoch)

    writer.add_scalar("Loss_Code/Train", train_loss_1, epoch)
    writer.add_scalar("Loss_Code/Valid", valid_loss_1, epoch)
    writer.add_scalar("Loss_Code/Test", test_loss_1, epoch)

    writer.add_scalar("Loss_OP/Train", train_loss_2, epoch)
    writer.add_scalar("Loss_OP/Valid", valid_loss_2, epoch)
    writer.add_scalar("Loss_OP/Test", test_loss_2, epoch)

    if valid_loss_1 < best_valid_loss:
        best_valid_loss = valid_loss_1
        torch.save(model.state_dict(), f'best_models/{experiment_name}.pt')

    translation, attention, target_op, attention_op = translate_sentence_mtl(src, CODE, CODE, OP, model, device)
    f = create_attention_figure(src, translation, attention)

    writer.add_figure("Attention", f, global_step=epoch, close=True)
    writer.add_text("Predicted", " ".join(translation), epoch)

    f = create_attention_figure(src, target_op, attention_op)
    writer.add_figure("OP/Attention", f, global_step=epoch, close=True)
    writer.add_text("OP/Predicted", " ".join(target_op), epoch)

    scores_valid, scores_valid_op = compute_metrics_mtl(valid_data, CODE, CODE, OP, model, device, train_metrics)
    scores_test, scores_test_op = compute_metrics_mtl(test_data, CODE, CODE, OP, model, device, train_metrics)

    valid_bleu_score = scores_valid['BLUE'] * 100
    test_bleu_score = scores_test['BLUE'] * 100
    writer.add_scalar("BLUE/Valid", valid_bleu_score, epoch)
    writer.add_scalar("BLUE/Test", test_bleu_score, epoch)
    _, valid_corr_score = scores_valid['Corrects']
    _, test_corr_score = scores_test['Corrects']
    writer.add_scalar("Correct/Valid", valid_corr_score * 100, epoch)
    writer.add_scalar("Correct/Test", test_corr_score * 100, epoch)

    valid_bleu_score_op = scores_valid_op['BLUE'] * 100
    test_bleu_score_op = scores_test_op['BLUE'] * 100
    writer.add_scalar("BLUE_OP/Valid", valid_bleu_score_op, epoch)
    writer.add_scalar("BLUE_OP/Test", test_bleu_score_op, epoch)
    _, valid_corr_score_op = scores_valid_op['Corrects']
    _, test_corr_score_op = scores_test_op['Corrects']
    writer.add_scalar("Correct_OP/Valid", valid_corr_score_op * 100, epoch)
    writer.add_scalar("Correct_OP/Test", test_corr_score_op * 100, epoch)

    if valid_corr_score > max_valid_corr:
        max_valid_corr = valid_corr_score
        torch.save(model.state_dict(), f'best_models/{experiment_name}_corr_best.pt')

    writer.flush()

writer.close()

model.load_state_dict(torch.load(f"{experiment_name}.pt"))
model.eval()
model.to(device)

example_idx = 1

src = train_data.examples[example_idx].src
trg = train_data.examples[example_idx].trg
op = train_data.examples[example_idx].op

print(f'src = {src}')
print(f'trg = {trg}')

translation, attention, translation_op, attention_op = translate_sentence_mtl(src, CODE, CODE, OP, model, device)

print(f'predicted trg = {translation}')
# print(f'Attention = {attention}')
display_attention(src, translation, attention)
display_attention(op, translation_op, attention_op)

eval_metrics = {"BLEU": bleu_score, "Corrects": corrects}

scores, scores_op = compute_metrics_mtl(test_data, CODE, CODE, OP, model, device, eval_metrics, max_len=max_lens[size],
                                        max_len_op=20)

bleu_score = scores['BLEU']
print(f'BLEU score = {bleu_score * 100:.2f}')

correct, p_correct = scores['Corrects']

print(f'Correct score = {correct}')
print(f'Correct % score = {p_correct * 100:.2f}')

bleu_score = scores_op['BLEU']
print(f'BLEU score = {bleu_score * 100:.2f}')

correct, p_correct = scores_op['Corrects']

print(f'Correct score = {correct}')
print(f'Correct % score = {p_correct * 100:.2f}')
