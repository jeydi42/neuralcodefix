import argparse
import json
from os import makedirs
from os.path import join, dirname, exists

from tqdm import tqdm

from datasets.tufano import MethodPairs, SourceAbstract


def combine_dataset(pairs_path, source_path, out_path):
    remove = ['before', 'after']
    pairs = MethodPairs(pairs_path)
    with open(out_path, "wt", encoding="utf8") as outf:
        for size in ['small', 'medium']:
            source_abstract = SourceAbstract(join(source_path, size))
            print(len(source_abstract))

            codes_before = set()
            i, t, n = 0, 0, 0
            for folder in tqdm(pairs):
                for file in folder:
                    for method in folder[file]:
                        example = folder[file][method]
                        t += 1
                        code_before = folder[file][method]['before'].strip()
                        code_after = folder[file][method]['after'].strip()
                        key = (code_before, code_after)
                        if key in codes_before:
                            i += 1
                            continue

                        codes_before.add(key)

                        if key in source_abstract.data:
                            example.update(source_abstract.data[key])
                            example['operations'] = " ".join(
                                [operation.replace(" ", "_") for operation in example['operations'].split("\n")])
                            for k in remove:
                                example.pop(k, None)
                            example['size'] = size
                            outf.write(json.dumps(example) + "\n")
                            n += 1

            print(f"{i} - {t} - {n}")


def split(project, out):
    path = dirname(out)
    a = join(path, project)
    if not exists(a):
        makedirs(join(path, project))
    out_train_small = join(path, project, "train_small.json")
    out_train_medium = join(path, project, "train_medium.json")
    out_eval_small = join(path, project, "eval_small.json")
    out_eval_medium = join(path, project, "eval_medium.json")
    out_test_small = join(path, project, "test_small.json")
    out_test_medium = join(path, project, "test_medium.json")
    with open(out, "rt", encoding="utf8") as inf, \
            open(out_train_small, "wt", encoding="utf8") as trainf_s, \
            open(out_train_medium, "wt", encoding="utf8") as train_m, \
            open(out_eval_small, "wt", encoding="utf8") as evalf_s, \
            open(out_eval_medium, "wt", encoding="utf8") as evalf_m, \
            open(out_test_small, "wt", encoding="utf8") as testf_s, \
            open(out_test_medium, "wt", encoding="utf8") as testf_m:
        for line in inf:
            obj = json.loads(line, encoding="utf8")
            set = obj['set']
            size = obj['size']
            if set == 'train':
                if size == 'small':
                    trainf_s.write(line)
                else:
                    train_m.write(line)
            elif set == 'eval':
                if size == 'small':
                    evalf_s.write(line)
                else:
                    evalf_m.write(line)
            elif set == "test":
                if size == 'small':
                    testf_s.write(line)
                else:
                    testf_m.write(line)
            else:
                raise ValueError


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--project", type=str)
    parser.add_argument("--pairs_path", type=str)
    parser.add_argument("--abstract_path", type=str)
    parser.add_argument("--out", type=str)

    args = parser.parse_args()

    combine_dataset(args.pairs_path, args.abstract_path, args.out)
    split(args.project, args.out)
