import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import torch


def translate_sentence(sentence, src_field, trg_field, model, device, max_len=100):
    model.eval()
    tokens = [token.lower() for token in sentence]

    tokens = [src_field.init_token] + tokens + [src_field.eos_token]

    src_indexes = [src_field.vocab.stoi[token] for token in tokens]

    src_tensor = torch.LongTensor(src_indexes).unsqueeze(1).to(device)

    src_len = torch.LongTensor([len(src_indexes)]).to(device)

    with torch.no_grad():
        encoder_outputs, hidden, cell = model.encoder(src_tensor, src_len)

    mask = model.create_mask(src_tensor)

    trg_indexes = [trg_field.vocab.stoi[trg_field.init_token]]

    attentions = torch.zeros(max_len, 1, len(src_indexes)).to(device)

    for i in range(max_len):

        trg_tensor = torch.LongTensor([trg_indexes[-1]]).to(device)

        with torch.no_grad():
            output, (hidden, cell), attention = model.decoder(trg_tensor, hidden, cell, encoder_outputs, mask)

        attentions[i] = attention

        pred_token = output.argmax(1).item()

        trg_indexes.append(pred_token)

        if pred_token == trg_field.vocab.stoi[trg_field.eos_token]:
            break

    trg_tokens = [trg_field.vocab.itos[i] for i in trg_indexes]

    return trg_tokens[1:], attentions[:len(trg_tokens) - 1]


def translate_sentence_mtl(sentence, src_field, trg_field_code, trg_field_op, model, device, max_len=100, max_len_op=50):
    model.eval()
    tokens = [token.lower() for token in sentence]

    tokens = [src_field.init_token] + tokens + [src_field.eos_token]

    src_indexes = [src_field.vocab.stoi[token] for token in tokens]

    src_tensor = torch.LongTensor(src_indexes).unsqueeze(1).to(device)

    src_len = torch.LongTensor([len(src_indexes)]).to(device)

    with torch.no_grad():
        encoder_outputs, hidden, cell = model.encoder(src_tensor, src_len)

    mask = model.create_mask(src_tensor)

    trg_indexes_code = [trg_field_code.vocab.stoi[trg_field_code.init_token]]
    trg_indexes_op = [trg_field_op.vocab.stoi[trg_field_op.init_token]]

    attentions = torch.zeros(max_len, 1, len(src_indexes)).to(device)
    attentions_op = torch.zeros(max_len_op, 1, len(src_indexes)).to(device)

    for i in range(max_len):

        trg_tensor_code = torch.LongTensor([trg_indexes_code[-1]]).to(device)

        with torch.no_grad():
            output, (hidden, cell), attention = model.decoder_code(trg_tensor_code, hidden, cell, encoder_outputs, mask)

        attentions[i] = attention

        pred_token = output.argmax(1).item()

        trg_indexes_code.append(pred_token)

        if pred_token == trg_field_code.vocab.stoi[trg_field_code.eos_token]:
            break

    for i in range(max_len_op):

        trg_tensor_op = torch.LongTensor([trg_indexes_op[-1]]).to(device)

        with torch.no_grad():
            output, (hidden, cell), attention = model.decoder_op(trg_tensor_op, hidden, cell, encoder_outputs, mask)

        attentions_op[i] = attention

        pred_token = output.argmax(1).item()

        trg_indexes_op.append(pred_token)

        if pred_token == trg_field_code.vocab.stoi[trg_field_code.eos_token]:
            break

    trg_tokens = [trg_field_code.vocab.itos[i] for i in trg_indexes_code]
    trg_tokens_op = [trg_field_op.vocab.itos[i] for i in trg_indexes_op]

    return trg_tokens[1:], attentions[:len(trg_tokens) - 1], trg_tokens_op[1:], attentions_op[:len(trg_tokens_op)-1]


def create_attention_figure(sentence, translation, attention):
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)

    attention = attention.squeeze(1).cpu().detach().numpy()

    _ = ax.matshow(attention, cmap='bone')

    ax.tick_params(labelsize=15)
    ax.set_xticklabels([''] + ['<sos>'] + [t.lower() for t in sentence] + ['<eos>'],
                       rotation=45)
    ax.set_yticklabels([''] + translation)

    ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))

    return fig


def display_attention(sentence, translation, attention):
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)

    attention = attention.squeeze(1).cpu().detach().numpy()

    _ = ax.matshow(attention, cmap='bone')

    ax.tick_params(labelsize=10)
    ax.set_xticklabels([''] + ['<sos>'] + [t.lower() for t in sentence] + ['<eos>'],
                       rotation=90)
    ax.set_yticklabels([''] + translation)

    ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))

    plt.show()
    plt.close()


def compute_metrics(data, src_field, trg_field, model, device, metrics, max_len):
    trgs = []
    pred_trgs = []

    scores = {k: 0 for k in metrics}

    for datum in data:
        src = vars(datum)['src']
        trg = vars(datum)['trg']

        pred_trg, _ = translate_sentence(src, src_field, trg_field, model, device, max_len)

        # cut off <eos> token
        pred_trg = pred_trg[:-1]

        pred_trgs.append(pred_trg)
        trgs.append([trg])

    for metric, metric_function in metrics.items():
        scores[metric] = metric_function(pred_trgs, trgs)

    return scores


def compute_metrics_mtl(data, src_field, trg_field_code, trg_field_op, model, device, metrics, max_len=100, max_len_op=50):
    trgs_code = []
    trgs_op = []
    pred_code = []
    pred_ops = []

    scores = {k: 0 for k in metrics}
    scores_op = {k: 0 for k in metrics}

    for datum in data:
        src = vars(datum)['src']
        trg = vars(datum)['trg']
        op = vars(datum)['op']

        pred_trg, _, pred_op, _ = translate_sentence_mtl(src, src_field, trg_field_code, trg_field_op, model, device, max_len, max_len_op)

        # cut off <eos> token
        pred_trg = pred_trg[:-1]

        pred_code.append(pred_trg)
        trgs_code.append([trg])

        pred_ops.append(pred_op)
        trgs_op.append([op])

    for metric, metric_function in metrics.items():
        scores[metric] = metric_function(pred_code, trgs_code)

    for metric, metric_function in metrics.items():
        scores_op[metric] = metric_function(pred_ops, trgs_op)

    return scores, scores_op
