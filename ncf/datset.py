from os.path import join

from torchtext.data import Field, TabularDataset, BucketIterator

SEED = 1337

project = "all"
size = "medium"
exp = "test"
experiment_name = f"{project}-{size}-{exp}"

dataset_path = f"datasets/tufano/{project}/"

CODE = Field(init_token='<sos>',
             eos_token='<eos>',
             lower=True,
             include_lengths=True)

OP = Field(init_token='<sos>',
             eos_token='<eos>',
             lower=True,
             include_lengths=True)

train_data = TabularDataset(
    path=join(dataset_path, f'train_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE),
            'operations': ('operations', OP)
            })

CODE.build_vocab(train_data, min_freq=1)
OP.build_vocab(train_data, min_freq=1)

valid_data = TabularDataset(
    path=join(dataset_path, f'eval_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE),
            'operations': ('operations', OP)
            })

test_data = TabularDataset(
    path=join(dataset_path, f'test_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE) ,
            'operations': ('operations', OP)
            })

train_iterator, valid_iterator, test_iterator = BucketIterator.splits(
    (train_data, valid_data, test_data),
    batch_size=1,
    sort_within_batch=True,
    sort_key=lambda x: len(x.src),
    shuffle=True)

train_set_x = set()
train_set_y = set()
dev_set_x = set()
dev_set_y = set()
test_set_x = set()
test_set_y = set()

max_len = 0
n = 0
for b in train_iterator:
    x, src_len = b.src
    y, _ = b.trg
    o, lo = b.operations
    n+=len(o)
    if len(o) > max_len:
        max_len = len(o)
    train_set_x.add(x)
    train_set_y.add(y)

print(max_len)
print(n/len(train_iterator))


for b in valid_iterator:
    x, src_len = b.src
    y, _ = b.trg
    o, lo = b.operations

    dev_set_x.add(x)
    dev_set_y.add(y)

for b in test_iterator:
    test_set_x.add(x)
    test_set_y.add(y)


intersection_train_dev_x = train_set_x.intersection(dev_set_x)
intersection_train_test_x = train_set_x.intersection(test_set_x)
print(len(intersection_train_dev_x))
print(len(intersection_train_test_x))

intersection_train_dev_y = train_set_y.intersection(dev_set_y)
intersection_train_test_y = train_set_y.intersection(test_set_y)
print(len(intersection_train_dev_y))
print(len(intersection_train_test_y))
