import random

import torch
from torch import nn


class Seq2Seq(nn.Module):
    def __init__(self, encoder, decoder, src_pad_idx, device):
        super().__init__()

        self.encoder = encoder
        self.decoder = decoder
        self.src_pad_idx = src_pad_idx
        self.device = device

    def create_mask(self, src):
        mask = (src != self.src_pad_idx).permute(1, 0)
        return mask

    def forward(self, src, src_len, trg, teacher_forcing_ratio=0.5):
        # src = [src len, batch size]
        # src_len = [batch size]
        # trg = [trg len, batch size]
        # teacher_forcing_ratio is probability to use teacher forcing
        # e.g. if teacher_forcing_ratio is 0.75 we use teacher forcing 75% of the time

        batch_size = src.shape[1]
        trg_len = trg.shape[0]
        trg_vocab_size = self.decoder.output_dim

        # tensor to store decoder outputs
        outputs = torch.zeros(trg_len, batch_size, trg_vocab_size).to(self.device)

        # encoder_outputs is all hidden states of the input sequence, back and forwards
        # hidden is the final forward and backward hidden states, passed through a linear layer
        encoder_outputs, hidden, cell = self.encoder(src, src_len)

        # first input to the decoder is the <sos> tokens
        input = trg[0, :]

        mask = self.create_mask(src)

        # mask = [batch size, src len]

        for t in range(1, trg_len):
            # insert input token embedding, previous hidden state, all encoder hidden states
            #  and mask
            # receive output tensor (predictions) and new hidden state
            output, (hidden, cell), attention = self.decoder(input, hidden, cell, encoder_outputs, mask)

            # place predictions in a tensor holding predictions for each token
            outputs[t] = output

            # decide if we are going to use teacher forcing or not
            teacher_force = random.random() < teacher_forcing_ratio

            # get the highest predicted token from our predictions
            top1 = output.argmax(1)

            # if teacher forcing, use actual next token as next input
            # if not, use predicted token
            input = trg[t] if teacher_force else top1

        return outputs


class Seq2MTLSeq(nn.Module):
    def __init__(self, encoder, decoder_code, decoder_op, src_pad_idx, device):
        super().__init__()

        self.encoder = encoder
        self.decoder_code = decoder_code
        self.decoder_op = decoder_op
        self.src_pad_idx = src_pad_idx
        self.device = device

    def create_mask(self, src):
        mask = (src != self.src_pad_idx).permute(1, 0)
        return mask

    def forward(self, src, src_len, trg_code, trg_op, teacher_forcing_ratio=0.3):
        # src = [src len, batch size]
        # src_len = [batch size]
        # trg = [trg len, batch size]
        # teacher_forcing_ratio is probability to use teacher forcing
        # e.g. if teacher_forcing_ratio is 0.75 we use teacher forcing 75% of the time

        batch_size = src.shape[1]
        trg_len_code = trg_code.shape[0]
        trg_vocab_size = self.decoder_code.output_dim
        trg_len_op = trg_op.shape[0]
        trg_op_vocab_size = self.decoder_op.output_dim

        # tensor to store decoder outputs
        outputs_code = torch.zeros(trg_len_code, batch_size, trg_vocab_size).to(self.device)
        outputs_op = torch.zeros(trg_len_op, batch_size, trg_op_vocab_size).to(self.device)

        # encoder_outputs is all hidden states of the input sequence, back and forwards
        # hidden is the final forward and backward hidden states, passed through a linear layer
        encoder_outputs, enc_hidden, enc_cell = self.encoder(src, src_len)

        # first input to the decoder is the <sos> tokens
        input = trg_code[0, :]

        mask = self.create_mask(src)

        # mask = [batch size, src len]

        hidden = enc_hidden
        cell = enc_cell
        for t in range(1, trg_len_code):
            # insert input token embedding, previous hidden state, all encoder hidden states
            #  and mask
            # receive output tensor (predictions) and new hidden state
            output, (hidden, cell), attention = self.decoder_code(input, hidden, cell, encoder_outputs, mask)

            # place predictions in a tensor holding predictions for each token
            outputs_code[t] = output

            # decide if we are going to use teacher forcing or not
            teacher_force = random.random() < teacher_forcing_ratio

            # get the highest predicted token from our predictions
            top1 = output.argmax(1)

            # if teacher forcing, use actual next token as next input
            # if not, use predicted token
            input = trg_code[t] if teacher_force else top1

        hidden_op = enc_hidden
        cell_op = enc_cell
        input_op = trg_op[0, :]
        for t in range(1, trg_len_op):
            # insert input token embedding, previous hidden state, all encoder hidden states
            #  and mask
            # receive output tensor (predictions) and new hidden state
            output_op, (hidden_op, cell_op), attention = self.decoder_op(input_op, hidden_op, cell_op, encoder_outputs, mask)

            # place predictions in a tensor holding predictions for each token
            outputs_op[t] = output_op

            # decide if we are going to use teacher forcing or not
            teacher_force = random.random() < teacher_forcing_ratio

            # get the highest predicted token from our predictions
            top1 = output_op.argmax(1)

            # if teacher forcing, use actual next token as next input
            # if not, use predicted token
            input_op = trg_op[t] if teacher_force else top1

        return outputs_code, outputs_op